﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorseguro;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

/**
 *
 * @author joaqf
 */
public class ServidorSeguro {

    
     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {



        System.out.println("Bienvenido a la programación");


        
        System.setProperty("javax.net.ssl.keyStore", "serverKey.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "servpass");
        
        System.setProperty("javax.net.ssl.trustStore", "clientTrustedCerts.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "clientpass");
        
        try {
            SSLServerSocketFactory sslFactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
            SSLServerSocket srvSocket = (SSLServerSocket) sslFactory.createServerSocket(4043);
            SSLSocket socketSsl=(SSLSocket) srvSocket.accept();
            
            DataInputStream dis= new DataInputStream(socketSsl.getInputStream());
            
            String mensaje= dis.readUTF();
            
            System.out.println(mensaje);
            
            
            System.out.println("Adios a todos");
            
            
        } catch (IOException ex) {
            Logger.getLogger(ServidorSeguro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
